<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear',function(){
  \Artisan::call('config:cache');
return "ada";
});


Route::group(['middleware' => 'auth'],function() {
    Route::get('','DashboardController@index')->name('dashboard')->middleware('kaprodi.auth');

    Route::resource('user','UserController')->middleware('kaprodi.auth');

    Route::resource('fakultas','FakultasController',['parameters' => [
        'fakultas'  => 'fakultas'
    ]])->except(['show'])->middleware('kaprodi.auth');

    Route::resource('prodi','ProdiController')->except(['show'])->middleware('kaprodi.auth');

    Route::resource('kelas','KelasController',['parameters' => [
        'kelas' => 'kelas'
    ]])->except(['show'])->middleware('kaprodi.auth');

    Route::resource('matakuliah','MatakuliahController')->middleware('kaprodi.auth');
    Route::resource('tahun-ajaran','TahunAjaranController')->except(['show'])->middleware('kaprodi.auth');
    Route::resource('kurikulum','KurikulumController')->except(['show'])->middleware('kaprodi.auth');
    Route::resource('jenis-ujian','JenisUjianController')->except(['show'])->middleware('kaprodi.auth');
    Route::resource('jadwal-ujian','JadwalUjianController')->except(['show'])->middleware('kaprodi.auth');
    Route::resource('arsip-soal','ArsipSoalController')->except(['edit','update']);
    Route::resource('arsip-nilai','ArsipNilaiController')->except(['edit','update']);

    // export pdf
    Route::get('arsip-soal/pdf/{id}','ArsipSoalController@exportPdf')->name('arsip_soal.pdf');
    Route::get('arsip-nilai/pdf/{id}','ArsipNilaiController@exportPdf')->name('arsip_nilai.pdf');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
