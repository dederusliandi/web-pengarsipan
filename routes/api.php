<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api', 'middleware' => 'cors'],function() {
    // auth
    Route::post('auth/login','AuthController@login');
    Route::get('auth/logout','AuthController@invalidateToken');
    Route::get('auth/refresh_token','AuthController@refreshToken'); 
    
    // get data
    Route::get('kurikulum','DataController@getKurikulum');
    Route::get('tahun_ajaran','DataController@getTahunAjaran');
    Route::get('jenis_ujian','DataController@getJenisUjian');
    Route::get('jadwal_ujian','DataController@getJadwalUjian');
    Route::get('matakuliah','DataController@getMataKuliah');
    Route::get('fakultas','DataController@getFakultas');    
    Route::get('prodi','DataController@getProdi');
    Route::post('arsip_nilai','DataController@postNilai')->middleware('jwt.auth');
    Route::post('arsip_soal','DataController@postSoal')->middleware('jwt.auth');
    Route::get('soal','DataController@getSoal')->middleware('jwt.auth');
    Route::get('nilai','DataController@getNilai')->middleware('jwt.auth');
    Route::get('notif-soal','DataController@getNotifSoal')->middleware('jwt.auth');
    Route::get('notif-nilai','DataController@getNotifNilai')->middleware('jwt.auth');
});
