<?php

return [
	'api' => [
		'error' => [
			'public' => 'An error occured',
			'not_coach' => 'Only coach can access this.',
			'form_input' => 'The all fields is required. ',
		]
	]
];