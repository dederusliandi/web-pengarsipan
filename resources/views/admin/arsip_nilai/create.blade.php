@extends('admin.layouts.app')

@section('title','Tambah Jadwal Ujian')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Tambah Arsip nilai
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('arsip-nilai.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('arsip-nilai.store') }}" enctype="multipart/form-data">
							@csrf
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_nilai') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Nilai</label>
									<input type="text" class="form-control" id="" placeholder="Kode Nilai" name="kode_nilai" value="{{ old('kode_nilai') }}">
									<span class="help-block">{{ $errors->first('kode_nilai') }}</span>
                                </div>
                                
                                @if($errors->any())
									<div class="form-group {{ $errors->has('nilai') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Nilai</label>
									<input type="file" class="form-control" id="" placeholder="nilai" name="nilai" value="{{ old('nilai') }}">
									<span class="help-block">{{ $errors->first('nilai') }}</span>
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_user') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">NIDN</label>
									<select name="id_user" id="" class="form-control">
										<option value="">NIDN</option>
                                        @foreach($dataUser as $user)
                                            <option value="{{ $user->id }}" {{ old('id_user') == $user->id ? 'selected' : '' }}>{{ $user->nomor_induk }} - {{ $user->nama_lengkap }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_user') }}</span>																				
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_kurikulum') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Kurikulum</label>
									<select name="id_kurikulum" id="" class="form-control">
										<option value="">Kode Kurikulum</option>
                                        @foreach($dataKurikulum as $kurikulum)
                                            <option value="{{ $kurikulum->id }}" {{ old('id_kurikulum') == $kurikulum->id ? 'selected' : '' }}>{{ $kurikulum->kode_kurikulum }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_kurikulum') }}</span>																				
								</div>
								
									 @if($errors->any())
									<div class="form-group {{ $errors->has('id_jadwal_ujian') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Jadwal Ujian</label>
									<select name="id_jadwal_ujian" id="" class="form-control">
										<option value="">Kode Jadwal Ujian</option>
                                        @foreach($dataJadwalUjian as $jadwal_ujian)
                                            <option value="{{ $jadwal_ujian->id }}" {{ old('id_jadwal_ujian') == $jadwal_ujian->id ? 'selected' : '' }}>{{ $jadwal_ujian->kode_jadwal_ujian }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_jadwal_ujian') }}</span>																				
								</div>
								
								
								 @if($errors->any())
									<div class="form-group {{ $errors->has('id_tahun_ajaran') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Tahun Ajaran</label>
									<select name="id_tahun_ajaran" id="" class="form-control">
										<option value="">Kode Tahun Ajaran</option>
                                        @foreach($dataTahunAjaran as $tahun_ajaran)
                                            <option value="{{ $tahun_ajaran->id }}" {{ old('id_tahun_ajaran') == $tahun_ajaran->id ? 'selected' : '' }}>{{ $tahun_ajaran->kode_tahun_ajaran }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_tahun_ajaran') }}</span>																				
                                </div>

                                 @if($errors->any())
									<div class="form-group {{ $errors->has('id_jenis_ujian') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Jenis Ujian</label>
									<select name="id_jenis_ujian" id="" class="form-control">
										<option value="">Jenis Ujian</option>
                                        @foreach($dataJenisUjian as $jenisUjian)
                                            <option value="{{ $jenisUjian->id }}" {{ old('id_jenis_ujian') == $jenisUjian->id ? 'selected' : '' }}>{{ $jenisUjian->jenis_ujian }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_jenis_ujian') }}</span>																				
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_matakuliah') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">kode matakuliah</label>
									<select name="id_matakuliah" id="" class="form-control">
										<option value="">kode matakuliah</option>
                                        @foreach($dataMatakuliah as $matakuliah)
                                            <option value="{{ $matakuliah->id }}" {{ old('id_matakuliah') == $matakuliah->id ? 'selected' : '' }}>{{ $matakuliah->kode_matakuliah }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_matakuliah') }}</span>																				
                                </div>

                                @if($errors->any())
                                    <div class="form-group {{ $errors->has('id_fakultas') ? 'has-error' : 'has-success' }}">
                                @else
                                    <div class="form-group">
                                @endif
                                    <label for="">kode fakultas</label>
                                    <select name="id_fakultas" id="" class="form-control">
                                        <option value="">kode fakultas</option>
                                        @foreach($dataFakultas as $fakultas)
                                            <option value="{{ $fakultas->id }}" {{ old('id_fakultas') == $fakultas->id ? 'selected' : '' }}>{{ $fakultas->kode_fakultas }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{ $errors->first('id_fakultas') }}</span>																				
                                </div>

                                @if($errors->any())
                                    <div class="form-group {{ $errors->has('id_prodi') ? 'has-error' : 'has-success' }}">
                                @else
                                    <div class="form-group">
                                @endif
                                    <label for="">Kode Prodi</label>
                                    <select name="id_prodi" id="" class="form-control">
                                        <option value="">Kode Prodi</option>
                                        @foreach($dataProdi as $prodi)
                                            <option value="{{ $prodi->id }}" {{ old('id_prodi') == $prodi->id ? 'selected' : '' }}>{{ $prodi->kode_prodi }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{ $errors->first('id_prodi') }}</span>																				
                                </div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection