@extends('admin.layouts.app')

@section('title','Arsip nilai')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Arsip nilai
                <small>admin akademik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            @if(auth::user()->role_id != 2)
                                <a href="{{ route('arsip-nilai.create') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> tambah data</button></a>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @include('admin._partial.flash_message')
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode Nilai</th>
                                        <th>Kode Kurikulum</th>
                                        <th>Kode Jenis Ujian</th>
                                        <th>Kode Matakuliah</th>
                                        <th>NIDN</th>
                                        <th>Kode Fakultas</th>
                                        <th>Kode Prodi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataArsipNilai as $arsipNilai)
                                        <tr>
                                            <td>{{ $arsipNilai->kode_nilai }}</td>
                                            <td>{{ $arsipNilai->kurikulum->kode_kurikulum }}</td>
                                            <td>{{ $arsipNilai->jenisUjian->kode_jenis_ujian }}</td>
                                            <td>{{ $arsipNilai->matakuliah->kode_matakuliah }}</td>
                                            <td>{{ $arsipNilai->user->nomor_induk }}</td>
                                            <td>{{ $arsipNilai->fakultas->kode_fakultas }}</td>
                                            <td>{{ $arsipNilai->prodi->kode_prodi }}</td>
                                            <td>
                                                <a data-toggle="tooltip" data-placement="bottom" title="detail" href="{{ route('arsip-nilai.show',$arsipNilai->id) }}"><button class="btn btn-sm btn-info"><i class="fa fa-th"></i></button></a>                                                
                                                <a data-toggle="tooltip" data-placement="bottom" title="download nilai" href="{{ route('arsip_nilai.pdf',$arsipNilai->id) }}"><button class="btn btn-sm btn-success"><i class="fa fa-download"></i></button></a>                                                
                                                @if(auth::user()->role_id != 2)
                                                    <form action="{{ route('arsip-nilai.destroy',$arsipNilai->id) }}" style="display:inline-block;" method="POST" id="deleteData_{{ $arsipNilai->id }}">
                                                        @csrf
                                                        {{ method_field('DELETE') }}
                                                        <button data-toggle="tooltip" data-placement="bottom" title="delete" class="btn btn-danger delete btn-sm" id="{{ $arsipNilai->id }}"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/dist/js/sweetalert.min.js') }}"></script>
    <script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });

        $(document).on('click','.delete',function(e){
            e.preventDefault();
            var idForm = $(this).attr('id');
            swal({
                title: "Apakah anda yakin akan menghapus data arsip nilai ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                $('#deleteData_'+idForm).submit();
                swal("Data Terhapus", {
                    icon: "success", 
                });
                } else {
                swal("Data Masih Tersimpan"); 
                }
            });
        });
    });
    </script>
@endsection