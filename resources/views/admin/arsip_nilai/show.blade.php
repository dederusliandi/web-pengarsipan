@extends('admin.layouts.app')

@section('title','Detail matakuliah')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Arsip Nilai
                <small>admin akademik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <a href="{{ route('arsip-nilai.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Dosen</th>
                                        <td>{{ $arsipNilai->user->nama_lengkap }}</td>
                                    </tr>
                                     <tr>
                                        <th>Kode Kurikulum</th>
                                        <td>{{ $arsipNilai->kurikulum->kode_kurikulum }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Jenis Ujian</th>
                                        <td>{{ $arsipNilai->jenisUjian->kode_jenis_ujian }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Tahun Ajaran</th>
                                        <td>{{ $arsipNilai->tahunAjaran->kode_tahun_ajaran }}</td>
                                    </tr>
                                     <tr>
                                        <th>Kode Jadwal Ujian</th>
                                        <td>{{ $arsipNilai->JadwalUjian->kode_jadwal_ujian }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Mata Kuliah</th>
                                    <td>{{ $arsipNilai->mataKuliah->kode_matakuliah }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Fakultas</th>
                                        <td>{{ $arsipNilai->fakultas->kode_fakultas }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Prodi</th>
                                        <td>{{ $arsipNilai->prodi->kode_prodi }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Nilai</th>
                                        <td>{{ $arsipNilai->kode_nilai }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nilai</th>
                                        <td>
                                            <img src="{{ asset('storage/nilai/'.$arsipNilai->nilai) }}" width="300px" alt="">
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection