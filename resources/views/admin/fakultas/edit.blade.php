@extends('admin.layouts.app')

@section('title','Tambah Fakultas')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit Fakultas
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('fakultas.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('fakultas.update',$fakultas->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $fakultas->id }}">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_fakultas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Fakultas</label>
									<input type="text" class="form-control" id="" placeholder="Kode Fakultas" name="kode_fakultas" value="{{ $errors->any() ? old('kode_fakultas') : $fakultas->kode_fakultas }}">
									<span class="help-block">{{ $errors->first('kode_fakultas') }}</span>
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('fakultas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Fakultas</label>
									<input type="text" class="form-control" id="" placeholder="Fakultas" name="fakultas" value="{{ $errors->any() ? old('fakultas') : $fakultas->fakultas }}">
									<span class="help-block">{{ $errors->first('fakultas') }}</span>										
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('id_user') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">NIDN</label>
									<select name="id_user" id="" class="form-control">
										<option value="">NIDN</option>
                                        @foreach($dataUser as $user)
                                            <option value="{{ $user->id }}" {{ $errors->any() ? (old('id_user') == $user->id ? 'selected' : '') : ($fakultas->id_user = $user->id ? 'selected' : '') }}>{{ $user->nomor_induk }} - {{ $user->nama_lengkap }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_user') }}</span>																				
								</div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection