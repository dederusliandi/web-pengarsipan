@extends('admin.layouts.app')

@section('title','Tambah Jenis Ujian')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit Jenis Ujian
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('jenis-ujian.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('jenis-ujian.update',$jenisUjian->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $jenisUjian->id }}">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_jenis_ujian') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Jenis Ujian</label>
									<input type="text" class="form-control" id="" placeholder="Kode Jenis Ujian" name="kode_jenis_ujian" value="{{ $errors->any() ? old('kode_jenis_ujian') : $jenisUjian->kode_jenis_ujian }}">
									<span class="help-block">{{ $errors->first('kode_jenis_ujian') }}</span>
                                </div>
                                
								@if($errors->any())
									<div class="form-group {{ $errors->has('jenis_ujian') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Jenis Ujian</label>
									<select name="jenis_ujian" id="" class="form-control">
                                        <option value="">Jenis Ujian</option>
                                        <option value="UTS" {{ $errors->any() ? (old('jenis_ujian') == "UTS" ? 'selected' : '') : ($jenisUjian->jenis_ujian == "UTS" ? 'selected' : '') }}>UTS</option>
                                        <option value="UAS" {{ $errors->any() ? (old('jenis_ujian') == "UAS" ? 'selected' : '') : ($jenisUjian->jenis_ujian == "UAS" ? 'selected' : '') }}>UAS</option>
                                        <option value="Susulan" {{ $errors->any() ? (old('jenis_ujian') == "Susulan" ? 'selected' : '') : ($jenisUjian->jenis_ujian == "Susulan" ? 'selected' : '') }}>Susulan</option>
                                    </select>
									<span class="help-block">{{ $errors->first('jenis_ujian') }}</span>										
                                </div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection