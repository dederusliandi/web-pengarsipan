@extends('admin.layouts.app')

@section('title','Tambah Prodi')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit Prodi
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('prodi.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('prodi.update',$prodi->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $prodi->id }}">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_prodi') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Prodi</label>
									<input type="text" class="form-control" id="" placeholder="Kode Prodi" name="kode_prodi" value="{{ $errors->any() ? old('kode_prodi') : $prodi->kode_prodi }}">
									<span class="help-block">{{ $errors->first('kode_prodi') }}</span>
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('prodi') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Prodi</label>
									<input type="text" class="form-control" id="" placeholder="Prodi" name="prodi" value="{{ $errors->any() ? old('prodi') : $prodi->prodi }}">
									<span class="help-block">{{ $errors->first('prodi') }}</span>										
                                </div>
                                
                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_fakultas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">kode fakultas</label>
									<select name="id_fakultas" id="" class="form-control">
										<option value="">kode fakultas</option>
                                        @foreach($dataFakultas as $fakultas)
                                            <option value="{{ $fakultas->id }}" {{ $errors->any() ? (old('id_fakultas') == $fakultas->id ? 'selected' : '') : ($prodi->id_fakultas == $fakultas->id ? 'selected' : '') }}>{{ $fakultas->kode_fakultas }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_fakultas') }}</span>																				
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('id_user') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">NIDN</label>
									<select name="id_user" id="" class="form-control">
										<option value="">NIDN</option>
                                        @foreach($dataUser as $user)
                                            <option value="{{ $user->id }}" {{ $errors->any() ? (old('id_user') == $user->id ? 'selected' : '') : ($prodi->id_user == $user->id ? 'selected' : '') }}>{{ $user->nomor_induk }} - {{ $user->nama_lengkap }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_user') }}</span>																				
								</div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection