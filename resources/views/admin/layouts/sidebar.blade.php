<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            @if(Auth::User()->role_id == 1)
                <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
                <li class="treeview {{ $page == 'user' || $page == 'fakultas' || $page == 'prodi' || $page == 'matakuliah' ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Data Master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ ($page == "user" ? 'active' : '') }}"><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> User</a></li>
                        <li class="{{ ($page == "fakultas" ? 'active' : '') }}"><a href="{{ route('fakultas.index') }}"><i class="fa fa-circle-o"></i> Fakultas</a></li>
                        <li class="{{ ($page == "prodi" ? 'active' : '') }}"><a href="{{ route('prodi.index') }}"><i class="fa fa-circle-o"></i> Prodi</a></li>
                        <li class="{{ ($page == "matakuliah" ? 'active' : '') }}"><a href="{{ route('matakuliah.index') }}"><i class="fa fa-circle-o"></i> Matakuliah</a></li>
                    </ul>
                </li>
                <li class="treeview {{ $page == 'tahun_ajaran' || $page == 'kurikulum' || $page == 'jenis_ujian' || $page == 'kelas' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-calendar"></i> <span>Kurikulum</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ $page == 'tahun_ajaran' ? 'active' :'' }}"><a href="{{ route('tahun-ajaran.index') }}"><i class="fa fa-circle-o"></i> Tahun Ajaran</a></li>
                        <li class="{{ $page == 'kurikulum' ? 'active' :'' }}"><a href="{{ route('kurikulum.index') }}"><i class="fa fa-circle-o"></i> Data Kurikulum</a></li>
                        <li class="{{ $page == 'jenis_ujian' ? 'active' :'' }}"><a href="{{ route('jenis-ujian.index') }}"><i class="fa fa-circle-o"></i> Jenis Ujian</a></li>
                        <li class="{{ $page == 'kelas' ? 'active' :'' }}"><a href="{{ route('kelas.index') }}"><i class="fa fa-circle-o"></i> Kelas</a></li>
                    </ul>
                </li>
                <li class="{{ ($page == 'jadwal_ujian' ? 'active' : '') }}"><a href="{{ route('jadwal-ujian.index') }}"><i class="fa fa-tasks"></i> <span>Jadwal Ujian</span></a></li>
            @endif
            <li class="{{ ($page == 'arsip_soal' ? 'active' : '') }}"><a href="{{ route('arsip-soal.index') }}"><i class="fa fa-folder"></i> <span>Soal</span></a></li>
            <li class="{{ ($page == 'arsip_nilai' ? 'active' : '') }}"><a href="{{ route('arsip-nilai.index') }}"><i class="fa fa-folder"></i> <span>Nilai</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>