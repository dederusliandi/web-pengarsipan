@extends('admin.layouts.app')

@section('title','kelas')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelas
                <small>admin akademik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <a href="{{ route('kelas.create') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> tambah data</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @include('admin._partial.flash_message')
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode Kelas</th>
                                        <th>Kelas</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataKelas as $kelas)
                                        <tr>
                                            <td>{{ $kelas->kode_kelas }}</td>
                                            <td>{{ $kelas->kelas }}</td>
                                            <td>
                                                <a data-toggle="tooltip" data-placement="bottom" title="edit" href="{{ route('kelas.edit',$kelas->id) }}"><button class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></button></a>
                                                <form action="{{ route('kelas.destroy',$kelas->id) }}" style="display:inline-block;" method="POST" id="deleteData_{{ $kelas->id }}">
                                                    @csrf
                                                    {{ method_field('DELETE') }}
                                                    <button data-toggle="tooltip" data-placement="bottom" title="delete" class="btn btn-danger btn-sm delete" id="{{ $kelas->id }}"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/dist/js/sweetalert.min.js') }}"></script>
    <script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });

        $(document).on('click','.delete',function(e){
            e.preventDefault();
            var idForm = $(this).attr('id');
            swal({
                title: "Apakah anda yakin akan menghapus data kelas ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                $('#deleteData_'+idForm).submit();
                swal("Data Terhapus", {
                    icon: "success", 
                });
                } else {
                swal("Data Masih Tersimpan"); 
                }
            });
        });
    });
    </script>
@endsection