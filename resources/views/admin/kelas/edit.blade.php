@extends('admin.layouts.app')

@section('title','Tambah kelas')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit kelas
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('kelas.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('kelas.update',$kelas->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $kelas->id }}">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_kelas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode kelas</label>
									<input type="text" class="form-control" id="" placeholder="Kode kelas" name="kode_kelas" value="{{ $errors->any() ? old('kode_kelas') : $kelas->kode_kelas }}">
									<span class="help-block">{{ $errors->first('kode_kelas') }}</span>
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('kelas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kelas</label>
									<select name="kelas" id="" class="form-control">
                                        <option value="">kelas</option>
                                        <option value="regular pagi" {{ $errors->any() ? (old('kelas') == "regular pagi" ? 'selected' : '') : ($kelas->kelas == "regular pagi" ? 'selected' : '') }}>regular pagi</option>
                                        <option value="regular malam" {{ $errors->any() ? (old('kelas') == "regular malam" ? 'selected' : '') : ($kelas->kelas == "regular malam" ? 'selected' : '') }}>regular malam</option>
                                        <option value="karyawan" {{ $errors->any() ? (old('kelas') == "karyawan" ? 'selected' : '') : ($kelas->kelas == "karyawan" ? 'selected' : '') }}>karyawan</option>
                                    </select>
									<span class="help-block">{{ $errors->first('kelas') }}</span>										
                                </div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection