@extends('admin.layouts.app')

@section('title','Tambah User')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Tambah User
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('user.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('user.store') }}" enctype="multipart/form-data">
							@csrf
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('nama_lengkap') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Nama lengkap</label>
									<input type="text" class="form-control" id="" placeholder="Nama Lengkap" name="nama_lengkap" value="{{ old('nama_lengkap') }}">
									<span class="help-block">{{ $errors->first('nama_lengkap') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('nomor_induk') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Nomor Induk</label>
									<input type="text" class="form-control" id="" placeholder="Nomor Induk" name="nomor_induk" value="{{ old('nomor_induk') }}">
									<span class="help-block">{{ $errors->first('nomor_induk') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Jenis Kelamin</label>
									<select name="jenis_kelamin" id="" class="form-control">
										<option value="">Jenis Kelamin</option>
										<option value="laki-laki" {{ old('jenis_kelamin') == "laki-laki" ? 'selected' : '' }}>Laki-laki</option>
										<option value="perempuan" {{ old('jenis_kelamin') == "perempuan" ? 'selected' : '' }}>Perempuan</option>
									</select>
									<span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('tempat_lahir') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">tempat lahir</label>
									<input type="text" class="form-control" id="" placeholder="Tempat Lahir" name="tempat_lahir" value="{{ old('tempat_lahir') }}">
									<span class="help-block">{{ $errors->first('tempat_lahir') }}</span>										
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Tanggal Lahir</label>
									<input type="date" class="form-control" name="tanggal_lahir" id="" placeholder="Tanggal Lahir" value="{{ old('tanggal_lahir') }}">
									<span class="help-block">{{ $errors->first('tanggal_lahir') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('agama') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Agama</label>
									<select name="agama" id="" class="form-control">
										<option value="">Agama</option>
										<option value="islam" {{ old('agama') == "islam" ? "selected" : "" }}>islam</option>
										<option value="kristen" {{ old('agama') == "kristen" ? "selected" : "" }}>kristen</option>
										<option value="budha" {{ old('agama') == "budha" ? "selected" : "" }}>budha</option>
										<option value="hindu" {{ old('agama') == "hindu" ? "selected" : "" }}>hindu</option>
									</select>
									<span class="help-block">{{ $errors->first('agama') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('telepon') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Telepon</label>
									<input type="text" class="form-control" id="" placeholder="Telepon" name="telepon" value="{{ old('telepon') }}">
									<span class="help-block">{{ $errors->first('telepon') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('alamat') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">alamat</label>
									<textarea name="alamat" id="" cols="30" rows="10" placeholder="alamat" class="form-control">{{ old('alamat') }}</textarea>
									<span class="help-block">{{ $errors->first('alamat') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('role_id') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Role</label>
									<select name="role_id" id="" placeholder="Role" class="form-control">
										<option value="">Role</option>
										@foreach($roles as $role)
									<option value="{{ $role->id }}" {{ $role->id == old('role_id') ? 'selected' : '' }}>{{ $role->role }}</option>
										@endforeach
									</select>
									<span class="help-block">{{ $errors->first('role_id') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Email</label>
									<input type="email" class="form-control" id="" placeholder="Email" name="email" value="{{ old('email') }}">
									<span class="help-block">{{ $errors->first('email') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('password') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Password</label>
									<input type="password" class="form-control" id="" placeholder="Password" name="password" value="{{ old('password') }}">
									<span class="help-block">{{ $errors->first('password') }}</span>																														
								</div>

								<div class="form-group">
									<label for="">Password Confirmation</label>
									<input type="password" class="form-control" id="" placeholder="Password Confirmation" name="password_confirmation">
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Foto</label>
									<input type="file" class="form-control" id="" placeholder="Foto" name="foto">
									<span class="help-block">{{ $errors->first('foto') }}</span>																												
								</div>
							</div>
							<!-- /.box-body -->

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection