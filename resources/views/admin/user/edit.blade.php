@extends('admin.layouts.app')

@section('title','Tambah User')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit User
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('user.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('user.update',$user->id) }}" enctype="multipart/form-data">
							@csrf
							{{ method_field('PATCH') }}
							<input type="hidden" value="{{ $user->id }}" name="id">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('nama_lengkap') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Nama lengkap</label>
									<input type="text" class="form-control" id="" placeholder="Nama Lengkap" name="nama_lengkap" value="{{ $errors->any() ? old('nama_lengkap') : $user->nama_lengkap }}">
									<span class="help-block">{{ $errors->first('nama_lengkap') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('nomor_induk') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Nomor Induk</label>
									<input type="text" class="form-control" id="" placeholder="Nomor Induk" name="nomor_induk" value="{{ $errors->any() ? old('nomor_induk') : $user->nomor_induk }}">
									<span class="help-block">{{ $errors->first('nomor_induk') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Jenis Kelamin</label>
									<select name="jenis_kelamin" id="" class="form-control">
										<option value="">Jenis Kelamin</option>
										<option value="laki-laki" {{ $errors->any() ? (old('jenis_kelamin') == "laki-laki" ? 'selected' : '') : ($user->profil->jenis_kelamin == "laki-laki" ? "selected" : "") }}>Laki-laki</option>
										<option value="perempuan" {{ $errors->any() ? (old('jenis_kelamin') == "perempuan" ? 'selected' : '') : ($user->profil->jenis_kelamin == "perempuan" ? "selected" : "") }}>Perempuan</option>
									</select>
									<span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('tempat_lahir') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">tempat lahir</label>
									<input type="text" class="form-control" id="" placeholder="Tempat Lahir" name="tempat_lahir" value="{{ $errors->any() ? old('tempat_lahir') : $user->profil->tempat_lahir }}">
									<span class="help-block">{{ $errors->first('tempat_lahir') }}</span>										
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Tanggal Lahir</label>
									<input type="date" class="form-control" name="tanggal_lahir" id="" placeholder="Tanggal Lahir" value="{{ $errors->any() ? old('tanggal_lahir') : $user->profil->tanggal_lahir }}">
									<span class="help-block">{{ $errors->first('tanggal_lahir') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('agama') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Agama</label>
									<select name="agama" id="" class="form-control">
										<option value="">Agama</option>
										<option value="islam" {{ $errors->any() ? (old('agama') == "islam" ? "selected" : "") : ($user->profil->agama == "islam" ? "selected" : "") }}>islam</option>
										<option value="kristen" {{ $errors->any() ? (old('agama') == "kristen" ? "selected" : "") : ($user->profil->agama == "kristen" ? "selected" : "") }}>kristen</option>
										<option value="budha" {{ $errors->any() ? (old('agama') == "budha" ? "selected" : "") : ($user->profil->agama == "budha" ? "selected" : "") }}>budha</option>
										<option value="hindu" {{ $errors->any() ? (old('agama') == "hindu" ? "selected" : "") : ($user->profil->agama == "hindu" ? "selected" : "") }}>hindu</option>
									</select>
									<span class="help-block">{{ $errors->first('agama') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('telepon') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Telepon</label>
									<input type="text" class="form-control" id="" placeholder="Telepon" name="telepon" value="{{ $errors->any() ? old('telepon') : $user->profil->telepon }}">
									<span class="help-block">{{ $errors->first('telepon') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('alamat') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">alamat</label>
									<textarea name="alamat" id="" cols="30" rows="10" placeholder="alamat" class="form-control">{{ $errors->any() ? old('alamat') : $user->profil->alamat }}</textarea>
									<span class="help-block">{{ $errors->first('alamat') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('role_id') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Role</label>
									<select name="role_id" id="" placeholder="Role" class="form-control">
										<option value="">Role</option>
										@foreach($roles as $role)
									<option value="{{ $role->id }}" {{ $errors->any() ? ($role->id == old('role_id') ? 'selected' : '') : ($user->role_id == $role->id ? "selected" : "") }}>{{ $role->role }}</option>
										@endforeach
									</select>
									<span class="help-block">{{ $errors->first('role_id') }}</span>																				
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Email</label>
									<input type="email" class="form-control" id="" placeholder="Email" name="email" value="{{ $errors->any() ? old('email') : $user->email }}">
									<span class="help-block">{{ $errors->first('email') }}</span>																												
								</div>

								@if($errors->any())
									<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Foto</label>
									<input type="file" class="form-control" id="" placeholder="Foto" name="foto">
									<span class="help-block">{{ $errors->first('foto') }}</span>																												
								</div>
							</div>
							<!-- /.box-body -->

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection