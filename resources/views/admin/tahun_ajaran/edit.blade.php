@extends('admin.layouts.app')

@section('title','Tambah kelas')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit Tahun Ajaran
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<a href="{{ route('tahun-ajaran.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('tahun-ajaran.update',$tahunAjaran->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $tahunAjaran->id }}">           
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_tahun_ajaran') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Tahun Ajaran</label>
									<input type="text" class="form-control" id="" placeholder="Kode Tahun Ajaran" name="kode_tahun_ajaran" value="{{ $errors->any() ? old('kode_tahun_ajaran') : $tahunAjaran->kode_tahun_ajaran }}">
									<span class="help-block">{{ $errors->first('kode_tahun_ajaran') }}</span>
								</div>
								
								@if($errors->any())
									<div class="form-group {{ $errors->has('tahun') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Tahun</label>
									<input type="text" class="form-control" id="" placeholder="Tahun" name="tahun" value="{{ $errors->any() ? old('tahun') : $tahunAjaran->tahun }}">
									<span class="help-block">{{ $errors->first('tahun') }}</span>
                                </div>
                                
								@if($errors->any())
									<div class="form-group {{ $errors->has('semester') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Semester</label>
									<select name="semester" id="" class="form-control">
                                        <option value="">semester</option>
                                        <option value="ganjil" {{ $errors->any() ? (old('semester') == "ganjil" ? 'selected' : '') : ($tahunAjaran->semester == "ganjil" ? 'selected' : '') }}>ganjil</option>
                                        <option value="genap" {{ $errors->any() ? (old('semester') == "genap" ? 'selected' : '') : ($tahunAjaran->semester == "genap" ? 'selected' : '') }}>genap</option>
                                    </select>
									<span class="help-block">{{ $errors->first('semester') }}</span>										
                                </div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection