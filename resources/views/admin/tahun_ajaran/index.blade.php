@extends('admin.layouts.app')

@section('title','Tahun Ajaran')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tahun Ajaran
                <small>admin akademik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <a href="{{ route('tahun-ajaran.create') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> tambah data</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @include('admin._partial.flash_message')
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode Tahun Ajaran</th>
                                        <th>Tahun</th>
                                        <th>Semester</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTahunAjaran as $tahunAjaran)
                                        <tr>
                                            <td>{{ $tahunAjaran->kode_tahun_ajaran }}</td>
                                            <td>{{ $tahunAjaran->tahun }}</td>
                                            <td>{{ $tahunAjaran->semester }}</td>
                                            <td>
                                                <a data-toggle="tooltip" data-placement="bottom" title="edit" href="{{ route('tahun-ajaran.edit',$tahunAjaran->id) }}"><button class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></button></a>
                                                <form action="{{ route('tahun-ajaran.destroy',$tahunAjaran->id) }}" style="display:inline-block;" method="POST" id="deleteData_{{ $tahunAjaran->id }}">
                                                    @csrf
                                                    {{ method_field('DELETE') }}
                                                    <button data-toggle="tooltip" data-placement="bottom" title="delete" class="btn btn-danger btn-sm delete" id="{{ $tahunAjaran->id }}"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/dist/js/sweetalert.min.js') }}"></script>
    <script>

    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });

        $(document).on('click','.delete',function(e){
            e.preventDefault();
            var idForm = $(this).attr('id');
            swal({
                title: "Apakah anda yakin akan menghapus data tahun ajaran ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                $('#deleteData_'+idForm).submit();
                swal("Data Terhapus", {
                    icon: "success", 
                });
                } else {
                swal("Data Masih Tersimpan"); 
                }
            });
        });
    });
    </script>
@endsection