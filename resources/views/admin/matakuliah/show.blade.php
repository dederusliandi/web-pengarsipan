@extends('admin.layouts.app')

@section('title','Detail matakuliah')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                matakuliah
                <small>admin akademik</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <a href="{{ route('matakuliah.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Kode Matakuliah</th>
                                        <td>{{ $matakuliah->kode_matakuliah }}</td>
                                    </tr>
                                     <tr>
                                        <th>Matakuliah</th>
                                        <td>{{ $matakuliah->matakuliah }}</td>
                                    </tr>
                                    <tr>
                                        <th>SKS</th>
                                    <td>{{ $matakuliah->sks }}</td>
                                    </tr>
                                    <tr>
                                        <th>Semester</th>
                                        <td>{{ $matakuliah->semester }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Prodi</th>
                                        <td>{{ $matakuliah->prodi->kode_prodi }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Fakultas</th>
                                        <td>{{ $matakuliah->fakultas->kode_fakultas }}</td>
                                    </tr>
                                    <tr>
                                        <th>NIDN</th>
                                        <td>{{ $matakuliah->user->nomor_induk }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Kelas</th>
                                        <td>{{ $matakuliah->kelas->kode_kelas }}</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection