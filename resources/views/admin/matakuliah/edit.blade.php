@extends('admin.layouts.app')

@section('title','Tambah Matakuliah')

@section('content')
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Edit Matakuliah
				<small>Admin Akademik</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Forms</a></li>
				<li class="active">General Elements</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header with-border">
				 			<a href="{{ route('matakuliah.index') }}"><button class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</button></a>
						</div>
						<!-- form start -->
						<form method="POST" role="form" action="{{ route('matakuliah.update',$matakuliah->id) }}">
                            @csrf
                            {{ method_field('PATCH') }}
                           <input type="hidden" name="id" value="{{ $matakuliah->id }}">
							<div class="box-body">
								@if($errors->any())
									<div class="form-group {{ $errors->has('kode_matakuliah') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Kode Matakuliah</label>
									<input type="text" class="form-control" id="" placeholder="Kode Matakuliah" name="kode_matakuliah" value="{{ $errors->any() ? old('kode_matakuliah') : $matakuliah->kode_matakuliah }}">
									<span class="help-block">{{ $errors->first('kode_matakuliah') }}</span>
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('matakuliah') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Matakuliah</label>
									<input type="text" class="form-control" id="" placeholder="matakuliah" name="matakuliah" value="{{ $errors->any() ? old('matakuliah') : $matakuliah->matakuliah }}">
									<span class="help-block">{{ $errors->first('matakuliah') }}</span>										
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('sks') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">SKS</label>
									<input type="text" class="form-control" id="" placeholder="sks" name="sks" value="{{ $errors->any() ? old('sks') : $matakuliah->sks }}">
									<span class="help-block">{{ $errors->first('sks') }}</span>										
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('semester') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">Semester</label>
									<select name="semester" id="" class="form-control">
										<option value="">semester</option>
                                        <option value="ganjil" {{ $errors->any() ? (old('semester') == "ganjil" ? 'selected' : '') : ($matakuliah->semester == "ganjil" ? 'selected' : '') }}>ganjil</option>
                                        <option value="genap" {{ $errors->any() ? (old('semester') == "genap" ? 'selected' : '') : ($matakuliah->semester == "genap" ? 'selected' : '') }}>genap</option>
									</select>
									<span class="help-block">{{ $errors->first('semester') }}</span>																				
                                </div>

                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_prodi') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">kode Prodi</label>
									<select name="id_prodi" id="" class="form-control">
										<option value="">kode Prodi</option>
                                        @foreach($dataProdi as $prodi)
                                            <option value="{{ $prodi->id }}" {{ $errors->any() ? (old('id_prodi') == $prodi->id ? 'selected' : '') : ($matakuliah->id_prodi == $prodi->id ? 'selected' : '') }}>{{ $prodi->kode_prodi }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_prodi') }}</span>																				
                                </div>

                                
                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_fakultas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">kode fakultas</label>
									<select name="id_fakultas" id="" class="form-control">
										<option value="">kode fakultas</option>
                                        @foreach($dataFakultas as $fakultas)
                                            <option value="{{ $fakultas->id }}" {{ $errors->any() ? (old('id_fakultas') == $fakultas->id ? 'selected' : '') : ($matakuliah->id_fakultas == $fakultas->id ? 'selected' : '') }}>{{ $fakultas->kode_fakultas }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_fakultas') }}</span>																				
                                </div>
                                

								@if($errors->any())
									<div class="form-group {{ $errors->has('id_user') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">NIDN</label>
									<select name="id_user" id="" class="form-control">
										<option value="">NIDN</option>
                                        @foreach($dataUser as $user)
                                            <option value="{{ $user->id }}" {{ $errors->any() ? (old('id_user') == $user->id ? 'selected' : '') : ($matakuliah->id_user == $user->id ? 'selected' : '') }}>{{ $user->nomor_induk }} - {{ $user->nama_lengkap }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_user') }}</span>																				
                                </div>
                                
                                @if($errors->any())
									<div class="form-group {{ $errors->has('id_kelas') ? 'has-error' : 'has-success' }}">
								@else
									<div class="form-group">
								@endif
									<label for="">kode kelas</label>
									<select name="id_kelas" id="" class="form-control">
										<option value="">kode kelas</option>
                                        @foreach($dataKelas as $kelas)
                                            <option value="{{ $kelas->id }}" {{ $errors->any() ? (old('id_kelas') == $kelas->id ? 'selected' : '') : ($matakuliah->id_kelas == $kelas->id ? 'selected' : '') }}>{{ $kelas->kode_kelas }}</option>
                                        @endforeach
									</select>
									<span class="help-block">{{ $errors->first('id_kelas') }}</span>																				
                                </div>

							<div class="box-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
@endsection