@extends('admin.layouts.app')

@section('title','Dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>{{ $user }}</h3>
                        <p>User</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                    <a href="{{ route('user.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $fakultas }}</h3>
                        <p>Fakultas</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                <a href="{{ route('fakultas.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $prodi }}</h3>
                        <p>Program Studi</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-th"></i>
                    </div>
                    <a href="{{ route('prodi.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
           
            <!-- ./col -->
        </div>

        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                    <h3>{{ $tahunAjaran }}</h3>
                        <p>Tahun Ajaran</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <a href="{{ route('tahun-ajaran.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $kurikulum }}</h3>
                        <p>Kurikulum</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                <a href="{{ route('kurikulum.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $jenisUjian }}</h3>
                        <p>Jenis Ujian</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cogs"></i>
                    </div>
                    <a href="{{ route('jenis-ujian.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

        <div class="row">
                        <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $kelas }}</h3>
                        <p>Kelas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-building"></i>
                    </div>
                    <a href="{{ route('kelas.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $kurikulum }}</h3>
                        <p>Matakuliah</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{ route('kurikulum.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $jadwalUjian }}</h3>
                        <p>Jadwal Ujian</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bell"></i>
                    </div>
                    <a href="{{ route('jadwal-ujian.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>


    <!-- /.content -->
</div>
    <!-- /.content-wrapper -->
@endsection

@section('script')
    <script src="{{asset('assets/amcharts/amcharts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/amcharts/serial.js')}}" type="text/javascript"></script>

    <script>
        var chart;

        var chartData = [
            {
                'date': '2016',
                'visitor': '70',
            },
            {
                'date': '2017',
                'visitor': '125',
            },
            {
                'date': '2018',
                'visitor': '85',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            },
            {
                'date': '',
                'visitor': '',
            }
            ];


        AmCharts.ready(function () {
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "date";
            chart.startDuration = 1;
            
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 30;
            categoryAxis.gridPosition = "start";

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visitor";
            graph.balloonText = "[[category]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            graph.fillColors = '#f47341';
            chart.addGraph(graph);

            chart.creditsPosition = "top-right";

            chart.write("chartdiv-visitor1");
        });
    </script>

    {{-- cart 2 --}}
    <script>
        var chart2;

        var chartData2 = [
            {
                'date': 'Jan',
                'visitor': '24',
            },
            {
                'date': 'Feb',
                'visitor': '35',
            },
            {
                'date': 'Mar',
                'visitor': '30',
            },
            {
                'date': 'Apr',
                'visitor': '15',
            },
            {
                'date': 'May',
                'visitor': '24',
            },
            {
                'date': 'Jun',
                'visitor': '15',
            },
            {
                'date': 'Jul',
                'visitor': '35',
            },
            {
                'date': 'Agust',
                'visitor': '30',
            },
            {
                'date': 'Sep',
                'visitor': '15',
            },
            {
                'date': 'Oct',
                'visitor': '24',
            },
            {
                'date': 'Nov',
                'visitor': '24',
            },
            {
                'date': 'Des',
                'visitor': '30',
            }
        ];


        AmCharts.ready(function () {
            // SERIAL CHART
            chart2 = new AmCharts.AmSerialChart();
            chart2.dataProvider = chartData2;
            chart2.categoryField = "date";
            chart2.startDuration = 1;
            
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 30;
            categoryAxis.gridPosition = "start";

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visitor";
            graph.balloonText = "[[category]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            graph.fillColors = '#f4417f';
            chart2.addGraph(graph);

            chart2.creditsPosition = "top-right";

            chart2.write("chartdiv-visitor2");
        });
    </script>
@endsection