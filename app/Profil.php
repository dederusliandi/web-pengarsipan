<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';

    protected $fillable = [
        'id_user',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'telepon',
        'alamat',
        'foto'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
