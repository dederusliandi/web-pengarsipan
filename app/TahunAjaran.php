<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table = 'tahun_ajaran';

    protected $fillable = [
        'kode_tahun_ajaran',
        'tahun',
        'semester'
    ];

    public function kurikulum()
    {
        return $this->hasMany('App\Kurikulum','id_tahun_ajaran','id');
    }

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_tahun_ajaran','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_tahun_ajaran','id');
    }

    public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_tahun_ajaran','id');
    }

    
}
