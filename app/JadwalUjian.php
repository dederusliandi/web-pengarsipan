<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalUjian extends Model
{
    protected $table = 'jadwal_ujian';

    protected $fillable = [
        'id_user',
        'id_fakultas',
        'id_matakuliah',
        'id_tahun_ajaran',
        'id_jenis_ujian',
        'id_kelas',
        'kode_jadwal_ujian',
        'tanggal_ujian'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function fakultas()
    {
        return $this->belongsTo('App\Fakultas','id_fakultas','id');
    }

    public function matakuliah()
    {
        return $this->belongsTo('App\Matakuliah','id_matakuliah','id');
    }

    public function tahunAjaran()
    {
        return $this->belongsTo('App\TahunAjaran','id_tahun_ajaran','id');
    }

    public function jenisUjian()
    {
        return $this->belongsTo('App\JenisUjian','id_jenis_ujian','id');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Kelas','id_kelas','id');
    }

    public function arsipSoal()
    {
        return $this->hasOne('App\ArsipSoal','id_jadwal_ujian','id');
    }

    public function arsipNilai()
    {
        return $this->hasOne('App\arsipNilai','id_jadwal_ujian','id');
    }
}
