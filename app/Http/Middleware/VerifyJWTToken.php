<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         try {
            if (!JWTAuth::parseToken()->authenticate()) return response()->json([
                'code' => 400,
                'message' => 'User not found',
            ], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'code' => $e->getStatusCode(),
                'message' => 'Token expired',
            ], $e->getStatusCode());
        } catch (\TAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'code' => $e->getStatusCode(),
                'message' => 'Token invalid',
            ], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'code' => $e->getStatusCode(),
                'message' => 'Token absent',
            ], $e->getStatusCode());
        }

        return $next($request);
    }
}
