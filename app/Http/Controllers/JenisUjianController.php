<?php

namespace App\Http\Controllers;

use App\JenisUjian;
use App\Http\Requests\JenisUjianRequest;
use Illuminate\Http\Request;

class JenisUjianController extends Controller
{

    public function index()
    {
        $dataJenisUjian = JenisUjian::orderBy('id','DESC')->get();
        return view('admin.jenis_ujian.index',compact('dataJenisUjian'));
    }

    public function create()
    {
        return view('admin.jenis_ujian.create');
    }

    public function store(JenisUjianRequest $request)
    {
        JenisUjian::create($request->all());
        return redirect()->route('jenis-ujian.index')->with('flash_message','tambah data jenis ujian berhasil');
    }

    public function edit(JenisUjian $jenisUjian)
    {
        return view('admin.jenis_ujian.edit',compact('jenisUjian'));
    }

    public function update(JenisUjianRequest $request, JenisUjian $jenisUjian)
    {
        $jenisUjian->update($request->all());
        return redirect()->route('jenis-ujian.index')->with('flash_message','update data jenis ujian berhasil');
    }

    public function destroy(JenisUjian $jenisUjian)
    {
        $jenisUjian->delete();
        return redirect()->route('jenis-ujian.index')->with('flash_message','hapus data jenis ujian berhasil');
    }
}
