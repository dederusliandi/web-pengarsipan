<?php

namespace App\Http\Controllers;

use App\Matakuliah;
use App\User;
use App\Fakultas;
use App\Prodi;
use App\Kelas;
use Illuminate\Http\Request;
use App\Http\Requests\MatakuliahRequest;

class MatakuliahController extends Controller
{

    public function index()
    {
        $dataMatakuliah = Matakuliah::orderBy('id','DESC')->get();
        return view('admin.matakuliah.index',compact('dataMatakuliah'));
    }

    public function create()
    {
        $dataUser       = User::where('role_id',3)->get();
        $dataFakultas   = Fakultas::all();
        $dataProdi      = Prodi::all();
        $dataKelas      = Kelas::all();
        return view('admin.matakuliah.create',compact('dataUser','dataFakultas','dataProdi','dataKelas'));
    }

    public function store(MatakuliahRequest $request)
    {
        Matakuliah::create($request->all());
        return redirect()->route('matakuliah.index')->with('flash_message','tambah data matakakuliah berhasil');
    }

    public function show(Matakuliah $matakuliah)
    {
        return view('admin.matakuliah.show',compact('matakuliah'));
    }

    public function edit(Matakuliah $matakuliah)
    {
        $dataUser       = User::where('role_id',3)->get();
        $dataFakultas   = Fakultas::all();
        $dataProdi      = Prodi::all();
        $dataKelas      = Kelas::all();
        return view('admin.matakuliah.edit',compact('matakuliah','dataUser','dataFakultas','dataProdi','dataKelas'));
    }

    public function update(MatakuliahRequest $request, Matakuliah $matakuliah)
    {
        $matakuliah->update($request->all());
        return redirect()->route('matakuliah.index')->with('flash_message','update data matakuliah berhasil');
    }

    public function destroy(Matakuliah $matakuliah)
    {
        $matakuliah->delete();
        return redirect()->route('matakuliah.index')->with('flash_message','hapus data matakuliah berhasil');
    }
}
