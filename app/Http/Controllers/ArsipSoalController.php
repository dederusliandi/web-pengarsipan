<?php

namespace App\Http\Controllers;

use App\ArsipSoal;
use App\User;
use App\Kurikulum;
use App\JenisUjian;
use App\Matakuliah;
use App\Fakultas;
use App\Prodi;
use App\TahunAjaran;
use App\JadwalUjian;
use File;
use App\Http\Requests\ArsipSoalRequest;
use Illuminate\Http\Request;

class ArsipSoalController extends Controller
{
    public function __construct()
    {
        $this->middleware('kaprodi.auth')->except('index','show','exportPdf');
    }
    
    public function index()
    {
        $dataArsipSoal = ArsipSoal::orderBy('id','DESC')->get();
        return view('admin.arsip_soal.index',compact('dataArsipSoal'));
    }

    public function create()
    {
        $dataUser       = User::where('role_id',3)->get();
        $dataKurikulum  = Kurikulum::all();
        $dataJenisUjian = JenisUjian::all();
        $dataMatakuliah = Matakuliah::all();
        $dataFakultas   = Fakultas::all();
        $dataProdi      = Prodi::all();
        $dataTahunAjaran = TahunAjaran::all();
        $dataJadwalUjian = JadwalUjian::all();
        
        return view('admin.arsip_soal.create',compact('dataUser','dataKurikulum','dataJenisUjian','dataMatakuliah','dataFakultas','dataProdi','dataTahunAjaran','dataJadwalUjian'));
    }

    public function store(ArsipSoalRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('soal'))
        {
            $file_name  = $request->soal->hashName();
            $input['soal'] = $file_name;
            $request->file('soal')->store('public/soal');
        }

        ArsipSoal::create($input);
        return redirect()->route('arsip-soal.index')->with('flash_message','tambah data arsip soal berhasil');
    }

    public function show(ArsipSoal $arsipSoal)
    {
        return view('admin.arsip_soal.show',compact('arsipSoal'));
    }

    public function destroy(ArsipSoal $arsipSoal)
    {
        File::delete(storage_path('app/public/soal/'.$arsipSoal->soal));
        $arsipSoal->delete();
        return redirect()->route('arsip-soal.index')->with('flash_message','hapus data arsip soal berhasil');
    }

    public function exportPdf($id)
    {
        $data = \App\ArsipSoal::find($id);
        $filename= asset('storage/soal/'.$data->soal);
        $cek = getimagesize($filename);
        $width  = $cek[0];
        $height = $cek[1];

        if($width > $height){
            $condition = 'landscape';
            $data['condition'] = 'width';
        } else {
            $condition = 'portrait';
            $data['condition'] = 'height';
        }

        $pdf = \PDF::loadView('pdf.arsip_soal', ["data" => $data])->setPaper('f4', $condition);
        return $pdf->download(date("Y-m-d H:i:s").'_arsip_soal.pdf');
    }
}
