<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Fakultas;
use App\Prodi;
use App\Matakuliah;
use App\TahunAjaran;
use App\JenisUjian;
use App\Kelas;
use App\JadwalUjian;
use App\Kurikulum;

class DashboardController extends Controller
{
    public function index()
    {
        $user       = User::count();
        $fakultas   = Fakultas::count();
        $prodi      = Prodi::count();
        $matakuliah = Matakuliah::count(); 
        $tahunAjaran= TahunAjaran::count();
        $kurikulum  = Kurikulum::count(); 
        $jenisUjian = JenisUjian::count();
        $kelas      = Kelas::count();
        $jadwalUjian = JadwalUjian::count();


        return view('admin.dashboard.index',compact('user','fakultas','prodi','matakuliah','tahunAjaran','kurikulum','jenisUjian','kelas','jadwalUjian'));
    }
}
