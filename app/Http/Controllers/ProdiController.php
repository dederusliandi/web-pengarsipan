<?php

namespace App\Http\Controllers;

use App\Prodi;
use App\Fakultas;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\ProdiRequest;

class ProdiController extends Controller
{
    public function index()
    {
        $dataProdi = Prodi::orderBy('id','DESC')->get();
        return view('admin.prodi.index',compact('dataProdi'));
    }

    public function create()
    {
        $dataFakultas   = Fakultas::all();
        $dataUser       = User::where('role_id',3)->get();
        return view('admin.prodi.create',compact('dataUser','dataFakultas'));
    }

    public function store(ProdiRequest $request)
    {
        Prodi::create($request->all());
        return redirect()->route('prodi.index')->with('flash_message','tambah data prodi berhasil');
    }

    public function edit(Prodi $prodi)
    {
        $dataFakultas   = Fakultas::all();
        $dataUser       = User::where('role_id',3)->get();
        return view('admin.prodi.edit',compact('prodi','dataFakultas','dataUser'));
    }

    public function update(ProdiRequest $request, Prodi $prodi)
    {
        $prodi->update($request->all());
        return redirect()->route('prodi.index')->with('flash_message','update data prodi berhasil');
    }

    public function destroy(Prodi $prodi)
    {
        $prodi->delete();
        return redirect()->route('prodi.index')->with('flash_message','hapus data prodi berhasil');
    }
}
