<?php

namespace App\Http\Controllers;

use App\TahunAjaran;
use Illuminate\Http\Request;
use App\Http\Requests\TahunAjaranRequest;

class TahunAjaranController extends Controller
{

    public function index()
    {
        $dataTahunAjaran = TahunAjaran::orderBy('id','DESC')->get();
        return view('admin.tahun_ajaran.index',compact('dataTahunAjaran'));
    }

    public function create()
    {
        return view('admin.tahun_ajaran.create');
    }

    public function store(TahunAjaranRequest $request)
    {
        TahunAjaran::create($request->all());
        return redirect()->route('tahun-ajaran.index')->with('flash_message','tambah data tahun ajaran berhasil');
    }

    public function edit(TahunAjaran $tahunAjaran)
    {
        return view('admin.tahun_ajaran.edit',compact('tahunAjaran'));
    }

    public function update(TahunAjaranRequest $request, TahunAjaran $tahunAjaran)
    {
        $tahunAjaran->update($request->all());
        return redirect()->route('tahun-ajaran.index')->with('flash_message','update data tahun ajaran berhasil');
    }

    public function destroy(TahunAjaran $tahunAjaran)
    {
        $tahunAjaran->delete();
        return redirect()->route('tahun-ajaran.index')->with('flash_message','hapus data tahun ajaran berhasil');
    }
}
