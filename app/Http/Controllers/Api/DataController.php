<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Kurikulum;
use App\JenisUjian;
use App\JadwalUjian;
use App\Matakuliah;
use App\TahunAjaran;
use App\Fakultas;
use App\Prodi;
use JWTAuth;
use DB;
use JWTAuthException;
use Validator;
use Illuminate\Support\Facades\Lang;
use App\ArsipNilai;
use App\ArsipSoal;
use App\Http\Controllers\Controller;

class DataController extends Controller
{
    public function getKurikulum()
    {
        $kurikulum = Kurikulum::select('id','kode_kurikulum')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all kurikulum';
        $response['data']   = $kurikulum;

        return response()->json($response);
    }

    public function getJenisUjian()
    {
        $jenisUjian = JenisUjian::select('id','kode_jenis_ujian')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all jenis ujian';
        $response['data']   = $jenisUjian;

        return response()->json($response);
    }

    public function getJadwalUjian()
    {
        $JadwalUjian = JadwalUjian::select('id','kode_jadwal_ujian')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all jadwal ujian';
        $response['data']   = $JadwalUjian;

        return response()->json($response);
    }

    public function getMataKuliah()
    {
        $matakuliah = Matakuliah::select('id','kode_matakuliah')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all matakuliah';
        $response['data']   = $matakuliah;

        return response()->json($response);
    }

    public function getFakultas()
    {
        $fakultas = Fakultas::select('id','kode_fakultas')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all fakultas';
        $response['data']   = $fakultas;

        return response()->json($response);
    }
    
    public function getTahunAjaran()
    {
        $tahunAjaran = TahunAjaran::select('id','kode_tahun_ajaran')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all tahun ajaran';
        $response['data']   = $tahunAjaran;

        return response()->json($response);
    }

    public function getProdi()
    {
        $prodi = Prodi::select('id','kode_prodi')->get();
        $response['code']   = 200;
        $response['message']    = 'list of all prodi';
        $response['data']   = $prodi;

        return response()->json($response);
    }

    public function postNilai(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $response = [
            'code'      => 400,
            'message'   => Lang::get('messages.api.error.public')
        ];

        $input = $request->all();

        $validator = Validator::make($input,[
            'id_kurikulum'      => 'required',
            'id_jenis_ujian'    => 'required',
            'id_tahun_ajaran'   => 'required',
            'id_jadwal_ujian'   => 'required',
            'id_matakuliah'     => 'required',
            'id_fakultas'       => 'required',
            'id_prodi'          => 'required',
            'kode_nilai'        => 'required|string|max:191|unique:arsip_nilai',
            'nilai'             => 'required|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            $response['message']    = Lang::get('messages.api.error.form_input');
            return response()->json($response);
        }
        // create arsip nilai
        $input['id_user']   = $user->id;

        if($request->hasFile('nilai'))
        {
            $file_name  = $request->nilai->hashName();
            $input['nilai'] = $file_name;
            $request->file('nilai')->store('public/nilai');
        }

        $arsipNilai = ArsipNilai::create($input);

        $response = [
            'code'      => 200,
            'message'   => 'arsip nilai created',
            'data'      => $arsipNilai
        ];

        return response()->json($response);   
    }

    public function postSoal(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $response = [
            'code'      => 400,
            'message'   => Lang::get('messages.api.error.public')
        ];

        $input = $request->all();

        $validator = Validator::make($input,[
            'id_kurikulum'      => 'required',
            'id_jenis_ujian'    => 'required',
            'id_tahun_ajaran'   => 'required',
            'id_jadwal_ujian'   => 'required',
            'id_matakuliah'     => 'required',
            'id_fakultas'       => 'required',
            'id_prodi'          => 'required',
            'kode_soal'         => 'required|string|max:191|unique:arsip_soal',
            'soal'              => 'required|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            $response['message']    = Lang::get('messages.api.error.form_input');
            return response()->json($response);
        }
        // create arsip soal
        $input['id_user']   = $user->id;
        if($request->hasFile('soal'))
        {
            $file_name  = $request->soal->hashName();
            $input['soal'] = $file_name;
            $request->file('soal')->store('public/soal');
        }

        $arsipSoal = ArsipSoal::create($input);

        $response = [
            'code'      => 200,
            'message'   => 'arsip soal created',
            'data'      => $arsipSoal
        ];

        return response()->json($response);   
    }

    public function getNilai()
    {
        $user = JWTAuth::parseToken()->authenticate();

        $nilai = ArsipNilai::select('id','kode_nilai','nilai',DB::raw('CONCAT("'.asset('storage/nilai').'/", nilai) as nilai_url'))->where('id_user',$user->id)->get();
        $response['code']   = 200;
        $response['message']    = 'list of all nilai';
        $response['data']   = $nilai;

        return response()->json($response);
    }

    public function getSoal()
    {
        $user = JWTAuth::parseToken()->authenticate();

        $soal = ArsipSoal::select('id','kode_soal','soal',DB::raw('CONCAT("'.asset('storage/soal').'/", soal) as soal_url'))->where('id_user',$user->id)->get();
        $response['code']   = 200;
        $response['message']    = 'list of all soal';
        $response['data']   = $soal;

        return response()->json($response);
    }

    public function getNotifSoal()
    {
        $user = JWTAuth::parseToken()->authenticate();

        $hasil = [];
        $datas = JadwalUjian::where('id_user',$user->id)->get();

        foreach($datas as $data)
        {
            if($data->arsipSoal == NULL){
                $hasil[] = [
                    "tanggal_ujian"     => $data->tanggal_ujian,
                    "tanggal_notif"     => date('Y-m-d', strtotime($data->tanggal_ujian. ' - 7 days')),
                    "matakuliah"  => $data->matakuliah->matakuliah,
                    "jenis_ujian"       => $data->jenisUjian->jenis_ujian
                ];
            }
        }
        $notif = [];

        foreach($hasil as $result){
            if(date('Y-m-d') == $result['tanggal_notif']){
                $notif[] = [
                    "description"   => "jangan lupa untuk mengumpulkan soal ".$result['jenis_ujian'].'untuk matakakuliah '.$result['matakuliah']
                ];
            }
        }

        $response['code']       = 200;
        $response['message']    = 'message notification soal';
        $response['data']       = $notif;

        return response()->json($response);    
    }

    public function getNotifNilai()
    {
        $user = JWTAuth::parseToken()->authenticate();

        $hasil = [];
        $datas = JadwalUjian::where('id_user',$user->id)->get();

        foreach($datas as $data)
        {
            if($data->arsipNilai == NULL){
                $hasil[] = [
                    "tanggal_ujian"     => $data->tanggal_ujian,
                    "tanggal_notif"     => date('Y-m-d', strtotime($data->tanggal_ujian. ' + 7 days')),
                    "matakuliah"        => $data->matakuliah->matakuliah,
                    "jenis_ujian"       => $data->jenisUjian->jenis_ujian
                ];
            }
        }
        $notif = [];

        foreach($hasil as $result){
            if(date('Y-m-d') == $result['tanggal_notif']){
                $notif[] = [
                    "description"   => "jangan lupa untuk mengumpulkan nilai ".$result['jenis_ujian'].' untuk matakakuliah '.$result['matakuliah']
                ];
            }
        }

        $response['code']       = 200;
        $response['message']    = 'message notification nilai';
        $response['data']       = $notif;

        return response()->json($response);    
    }
}
