<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use JWTAuth;
use JWTAuthException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request, User $user)
    {
        $response = [
            'code'      => 400,
            'message'   => Lang::get('messages.api.error.public')
        ];

        // check body request
        $validator = Validator::make($request->all(),[
            'nomor_induk'   => 'required',
            'password'      => 'required'
        ]);

        if($validator->fails()){
            $response['message'] = Lang::get('messages.api.error.form_input');
            return response()->json($response);
        }

        // nomor induk not found
        if(!$user = User::where('nomor_induk',$request->nomor_induk)->first()) return response()->json($response);

        // do login jwt auth
        $token = null;
        $credential = [
            'nomor_induk'   => $request->nomor_induk,
            'password'      => $request->password
        ];

         try {
            if(!$token = JWTAuth::attempt($credential)) {
                $response['code'] = 401; 
                $response['message'] = 'Nomor Induk or Password are incorrect';

                return response()->json($response);
            }
        }catch(JWTAuthException $e) {
            $response['message'] = 'Failed to create token';

            return response()->json($response);
        }

        // send response success
        $response['code']   = 200;
        $response['message']    = 'user signin';
        $response['data']   = $user;
        $response['access_token']   = $token;
        
        return response()->json($response);
    }

     public function invalidateToken()
    {
        // get token
        $token = JWTAuth::getToken();

        try {
            // do invalidate token ...
            JWTAuth::invalidate($token);   
        } catch (JWTException $e) {
            response()->json([
                'code' => 200,
                'message' => $e->getMessage(),
            ]);            
        }

        return response()->json([
            'code' => 200,
            'message' => 'Success to invalidate token',
        ]);
    }

    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        if(!$token){
            throw new BadRequestHtttpException('Token not provided');
        }
        try{
            $token = JWTAuth::refresh($token);
        }catch(TokenInvalidException $e){
            throw new AccessDeniedHttpException('The token is invalid');
        }

        return response()->json([
            'code' => 200,
            'message' => 'success to refresh token...',
            'access_token' => $token
        ]);
    }
}
