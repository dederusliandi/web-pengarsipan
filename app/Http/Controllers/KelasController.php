<?php

namespace App\Http\Controllers;

use App\Kelas;
use App\Http\Requests\KelasRequest;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index()
    {
        $dataKelas = Kelas::orderBy('id','DESC')->get();
        return view('admin.kelas.index',compact('dataKelas'));
    }

    public function create()
    {
        return view('admin.kelas.create');
    }

    public function store(KelasRequest $request)
    {
        Kelas::create($request->all());
        return redirect()->route('kelas.index')->with('flash_message','tambah data kelas berhasil');
    }

    public function edit(Kelas $kelas)
    {
        return view('admin.kelas.edit',compact('kelas'));
    }

    public function update(KelasRequest $request, Kelas $kelas)
    {
        $kelas->update($request->all());
        return redirect()->route('kelas.index')->with('flash_message','update data kelas berhasil');
    }

    public function destroy(Kelas $kelas)
    {
        $kelas->delete();
        return redirect()->route('kelas.index')->with('flash_message','hapus data kelas berhasil');
    }
}
