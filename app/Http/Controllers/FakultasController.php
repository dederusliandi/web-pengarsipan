<?php

namespace App\Http\Controllers;

use App\Fakultas;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\FakultasRequest;

class FakultasController extends Controller
{

    public function index()
    {
        $dataFakultas = Fakultas::orderBy('id','DESC')->get();
        return view('admin.fakultas.index',compact('dataFakultas'));
    }

    public function create()
    {
        $dataUser = User::where('role_id',3)->get();
        return view('admin.fakultas.create',compact('dataUser'));
    }

    public function store(FakultasRequest $request)
    {
        Fakultas::create($request->all());
        return redirect()->route('fakultas.index')->with('flash_message','tambah data fakultas berhasil');
    }

    public function edit(Fakultas $fakultas)
    {
        $dataUser = User::where('role_id',3)->get();
        return view('admin.fakultas.edit',compact('fakultas','dataUser'));
    }

    public function update(FakultasRequest $request, Fakultas $fakultas)
    {
        $fakultas->update($request->all());
        return redirect()->route('fakultas.index')->with('flash_message','update data fakultas berhasil');
    }

    public function destroy(Fakultas $fakultas)
    {
        $fakultas->delete();
        return redirect()->route('fakultas.index')->with('flash_message','hapus data fakultas berhasil');
    }
}
