<?php

namespace App\Http\Controllers;

use App\ArsipNilai;
use App\User;
use App\Kurikulum;
use App\JenisUjian;
use App\Matakuliah;
use App\Fakultas;
use App\Prodi;
use File;
use App\TahunAjaran;
use App\JadwalUjian;
use Illuminate\Http\Request;
use App\Http\Requests\ArsipNilaiRequest;

class ArsipNilaiController extends Controller
{

    public function __construct()
    {
        $this->middleware('kaprodi.auth')->except('index','show','exportPdf');
    }

     public function index()
    {
        $dataArsipNilai = ArsipNilai::orderBy('id','DESC')->get();
        return view('admin.arsip_nilai.index',compact('dataArsipNilai'));
    }

    public function create()
    {
        $dataUser       = User::where('role_id',3)->get();
        $dataKurikulum  = Kurikulum::all();
        $dataJenisUjian = JenisUjian::all();
        $dataMatakuliah = Matakuliah::all();
        $dataFakultas   = Fakultas::all();
        $dataProdi      = Prodi::all();
        $dataTahunAjaran = TahunAjaran::all();
        $dataJadwalUjian = JadwalUjian::all();
        
        return view('admin.arsip_nilai.create',compact('dataUser','dataKurikulum','dataJenisUjian','dataMatakuliah','dataFakultas','dataProdi','dataTahunAjaran','dataJadwalUjian'));
    }

    public function store(ArsipNilaiRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('nilai'))
        {
            $file_name  = $request->nilai->hashName();
            $input['nilai'] = $file_name;
            $request->file('nilai')->store('public/nilai');
        }

        ArsipNilai::create($input);
        return redirect()->route('arsip-nilai.index')->with('flash_message','tambah data arsip nilai berhasil');
    }

    public function show(ArsipNilai $arsipNilai)
    {
        return view('admin.arsip_nilai.show',compact('arsipNilai'));
    }

    public function destroy(ArsipNilai $arsipNilai)
    {
        File::delete(storage_path('app/public/nilai/'.$arsipNilai->nilai));
        $arsipNilai->delete();
        return redirect()->route('arsip-nilai.index')->with('flash_message','hapus data arsip nilai berhasil');
    }

    public function exportPdf($id)
    {
        $data = \App\ArsipNilai::find($id);
        $filename= asset('storage/Nilai/'.$data->nilai);
        $cek = getimagesize($filename);
        $width  = $cek[0];
        $height = $cek[1];

        if($width > $height){
            $condition          = 'landscape';
            $data['condition']  = 'width';
        } else {
            $condition          = 'portrait';
            $data['condition'] = 'height';
        }

        // return $data->condition;

        $pdf = \PDF::loadView('pdf.arsip_nilai', ["data" => $data])->setPaper('f4', $condition);
        return $pdf->download(date("Y-m-d H:i:s").'_arsip_nilai.pdf');
    }
}
