<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use App\Profil;
use File;

class UserController extends Controller
{

    public function index()
    {
        $users = User::orderBy('id','DESC')->get();
        return view('admin.user.index',compact('users'));
    }

    public function create()
    {
        $roles = \App\Role::all();
        return view('admin.user.create',compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $input              = $request->all();
        $input['password']  = bcrypt($request->password);
        $user               = User::create($input);
        $input['id_user']   = $user->id;

        if($request->hasFile('foto'))
        {
            $file_name  = $request->foto->hashName();
            $input['foto'] = $file_name;
            $request->file('foto')->store('public/profil');
        }

        Profil::create($input);
        return redirect()->route('user.index')->with('flash_message','tambah data user berhasil');
    }

    public function show(User $user)
    {
        return view('admin.user.show',compact('user'));
    }

    public function edit(User $user)
    {
        $roles = \App\Role::all();
        return view('admin.user.edit',compact('user','roles'));
    }

    public function update(UserRequest $request, User $user)
    {
        $input = $request->all();

        $user->update($input);

        if($request->hasFile('foto'))
        {
            File::delete(storage_path('app/public/profil/'.$user->profil->foto));
            $file_name      = $request->foto->hashName();
            $input['foto'] = $file_name;
            $request->file('foto')->store('public/profil');
        }

        $user->profil->update($input);

        return redirect()->route('user.index')->with('flash_message','update data user berhasil');
    }

    public function destroy(User $user)
    {
        File::delete(storage_path('app/public/profil/'.$user->profil->foto));
        $user->delete();
        return redirect()->route('user.index')->with('flash_message','hapus data user berhasil');
    }
}
