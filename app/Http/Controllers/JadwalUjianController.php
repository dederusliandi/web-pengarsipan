<?php

namespace App\Http\Controllers;

use App\JadwalUjian;
use App\User;
use App\Fakultas;
use App\Matakuliah;
use App\TahunAjaran;
use App\JenisUjian;
use App\Kelas;
use App\Http\Requests\JadwalUjianRequest;
use Illuminate\Http\Request;

class JadwalUjianController extends Controller
{

    public function index()
    {
        $dataJadwalUjian = JadwalUjian::orderBy('id','DESC')->get();
        return view('admin.jadwal_ujian.index',compact('dataJadwalUjian'));
    }

    public function create()
    {
        $dataUser           = User::where('role_id',3)->get();
        $dataFakultas       = Fakultas::all();
        $dataMatakuliah     = Matakuliah::all();
        $dataTahunAjaran    = TahunAjaran::all();
        $dataJenisUjian     = JenisUjian::all();
        $dataKelas          = Kelas::all();

        return view('admin.jadwal_ujian.create',compact('dataUser','dataFakultas','dataMatakuliah','dataTahunAjaran','dataJenisUjian','dataKelas'));
    }

    public function store(JadwalUjianRequest $request)
    {
        JadwalUjian::create($request->all());
        return redirect()->route('jadwal-ujian.index')->with('flash_message','tambah data jadwal ujian berhasil');
    }

    public function edit(JadwalUjian $jadwalUjian)
    {
        $dataUser           = User::where('role_id',3)->get();
        $dataFakultas       = Fakultas::all();
        $dataMatakuliah     = Matakuliah::all();
        $dataTahunAjaran    = TahunAjaran::all();
        $dataJenisUjian     = JenisUjian::all();
        $dataKelas          = Kelas::all();

        return view('admin.jadwal_ujian.edit',compact('jadwalUjian','dataUser','dataFakultas','dataMatakuliah','dataTahunAjaran','dataJenisUjian','dataKelas'));
    }

    public function update(JadwalUjianRequest $request, JadwalUjian $jadwalUjian)
    {
        $jadwalUjian->update($request->all());
        return redirect()->route('jadwal-ujian.index')->with('flash_message','update data jadwal ujian berhasil');
    }

    public function destroy(JadwalUjian $jadwalUjian)
    {
        $jadwalUjian->delete();
        return redirect()->route('jadwal-ujian.index')->with('flash_message','hapus data jadwal ujian berhasil');
    }
}
