<?php

namespace App\Http\Controllers;

use App\Kurikulum;
use App\User;
use App\Fakultas;
use App\Matakuliah;
use App\TahunAjaran;
use App\Kelas;
use Illuminate\Http\Request;
use App\Http\Requests\KurikulumRequest;

class KurikulumController extends Controller
{

    public function index()
    {
        $dataKurikulum = Kurikulum::orderBy('id','DESC')->get();
        return view('admin.kurikulum.index',compact('dataKurikulum'));
    }

    public function create()
    {
        $dataUser           = User::where('role_id',3)->get();
        $dataFakultas       = Fakultas::all();
        $dataMatakuliah     = Matakuliah::all();
        $dataTahunAjaran    = TahunAjaran::all();
        $dataKelas          = Kelas::all();
        return view('admin.kurikulum.create',compact('dataUser','dataFakultas','dataMatakuliah','dataTahunAjaran','dataKelas'));
    }

    public function store(KurikulumRequest $request)
    {
        Kurikulum::create($request->all());
        return redirect()->route('kurikulum.index')->with('flash_message','tambah data kurikulum berhasil');
    }

    public function show(Kurikulum $kurikulum)
    {
        return view('admin.kurikulum.show',compact('kurikulum'));
    }

    public function edit(Kurikulum $kurikulum)
    {
        $dataUser           = User::where('role_id',3)->get();
        $dataFakultas       = Fakultas::all();
        $dataMatakuliah     = Matakuliah::all();
        $dataTahunAjaran    = TahunAjaran::all();
        $dataKelas          = Kelas::all();

        return view('admin.kurikulum.edit',compact('kurikulum','dataUser','dataFakultas','dataMatakuliah','dataTahunAjaran','dataKelas'));
    }

    public function update(KurikulumRequest $request, Kurikulum $kurikulum)
    {
        $kurikulum->update($request->all());
        return redirect()->route('kurikulum.index')->with('flash_message','update data kurikulum berhasil');
    }

    public function destroy(Kurikulum $kurikulum)
    {
        $kurikulum->delete();
        return redirect()->route('kurikulum.index')->with('flash_message','hapus data kurikulum berhasil');
    }
}
