<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FakultasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeFakultas = 'required|unique:fakultas,kode_fakultas,'.$this->get('id');
        } else {
            $kodeFakultas = 'required|unique:fakultas';
        }
        
        return [
            'id_user'           => 'required',
            'kode_fakultas'     => $kodeFakultas,
            'fakultas'          => 'required|string|max:191'
        ];
    }
}
