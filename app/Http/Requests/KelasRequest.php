<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KelasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         if($this->method() == 'PATCH') {
            $kodeKelas = 'required|string|max:191|unique:kelas,kode_kelas,'.$this->get('id');
        } else {
            $kodeKelas = 'required|string|max:191|unique:kelas';
        }

        return [
            'kode_kelas'    => $kodeKelas,
            'kelas'             => 'required|string|max:191',
        ];
    }
}
