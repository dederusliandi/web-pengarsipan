<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArsipNilaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_user'           => 'required',
            'id_kurikulum'      => 'required',
            'id_jenis_ujian'    => 'required',
            'id_tahun_ajaran'   => 'required',
            'id_jadwal_ujian'   => 'required',
            'id_matakuliah'     => 'required',
            'id_fakultas'       => 'required',
            'id_prodi'          => 'required',
            'kode_nilai'        => 'required|string|max:191|unique:arsip_nilai',
            'nilai'             => 'required|image|mimes:jpeg,jpg,bmp,png'
        ];
    }
}
