<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JenisUjianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeJenisUjian = 'required|string|max:191|unique:jenis_ujian,kode_jenis_ujian,'.$this->get('id');
        } else {
            $kodeJenisUjian = 'required|string|max:191|unique:jenis_ujian';
        }
        return [
            'kode_jenis_ujian'  => $kodeJenisUjian,
            'jenis_ujian'       => 'required|in:UTS,UAS,Susulan'
        ];
    }
}
