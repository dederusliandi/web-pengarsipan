<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KurikulumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeKurikulum = 'required|string|max:191|unique:kurikulum,kode_kurikulum,'.$this->get('id');
        } else {
            $kodeKurikulum = 'required|string|max:191|unique:kurikulum';
        }
        return [
            'id_user'           => 'required',
            'id_matakuliah'     => 'required',
            'id_fakultas'       => 'required',
            'id_tahun_ajaran'   => 'required',
            'id_kelas'          => 'required',
            'kode_kurikulum'    => $kodeKurikulum,
        ];
    }
}
