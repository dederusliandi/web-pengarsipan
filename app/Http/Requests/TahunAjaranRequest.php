<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TahunAjaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeTahunAjaran = 'required|string|max:191|unique:tahun_ajaran,kode_tahun_ajaran,'.$this->get('id');
        } else {
            $kodeTahunAjaran = 'required|string|max:191|unique:tahun_ajaran';
        }
        
        return [
            'kode_tahun_ajaran' => $kodeTahunAjaran,
            'tahun'             => 'required|max:191',
            'semester'          => 'required|in:ganjil,genap'
        ];
    }
}
