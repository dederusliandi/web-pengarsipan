<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH')
        {
            $image      = 'sometimes|image|mimes:jpeg,jpg,bmp,png';
            $email      = 'required|string|email|max:191|unique:users,email,'.$this->get('id');
            $nomorInduk = 'required|string|max:191|unique:users,nomor_induk,'.$this->get('id');
            $password   = '';
        }
        else
        {
            $image      = 'required|image|mimes:jpeg,jpg,bmp,png';
            $email      = 'required|string|email|max:191|unique:users';
            $nomorInduk = 'required|string|max:191|unique:users';
            $password   = 'required|string|min:6|confirmed';
        }

        return [
            'nama_lengkap'      => 'required|string|max:191',
            'nomor_induk'       => $nomorInduk,
            'email'             => $email,
            'password'          => $password,
            'role_id'           => 'required',
            'jenis_kelamin'     => 'required|in:laki-laki,perempuan',
            'tempat_lahir'      => 'required|string|max:191',
            'tanggal_lahir'     => 'required|date',
            'agama'             => 'required|string',
            'telepon'           => 'required|numeric',
            'alamat'            => 'required|string',     
            'foto'              => $image
        ];
    }
}
