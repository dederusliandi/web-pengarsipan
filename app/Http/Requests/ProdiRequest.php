<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeProdi = 'required|string|max:191|unique:prodi,kode_prodi,'.$this->get('id');
        } else {
            $kodeProdi = 'required|string|max:191|unique:prodi';
        }   
        return [
            'id_user'       => 'required',
            'id_fakultas'   => 'required',
            'kode_prodi'    => $kodeProdi,
            'prodi'         => 'required|string|max:191'
        ];
    }
}
