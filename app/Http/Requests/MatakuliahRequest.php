<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatakuliahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeMatakuliah = 'required|string|max:191|unique:matakuliah,kode_matakuliah,'.$this->get('id');
        } else {
            $kodeMatakuliah = 'required|string|max:191|unique:matakuliah';
        }

        return [
            'id_user'           => 'required',
            'id_prodi'          => 'required',
            'id_fakultas'       => 'required',
            'id_kelas'          => 'required',
            'kode_matakuliah'   => $kodeMatakuliah,
            'matakuliah'        => 'required|string|max:191',
            'sks'               => 'required',
            'semester'          => 'required|in:ganjil,genap'
        ];
    }
}
