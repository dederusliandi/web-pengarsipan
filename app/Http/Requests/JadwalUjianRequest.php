<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JadwalUjianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH') {
            $kodeJadwalUjian = 'required|string|max:191|unique:jadwal_ujian,kode_jadwal_ujian,'.$this->get('id');
        } else {
            $kodeJadwalUjian = 'required|string|max:191|unique:jadwal_ujian';
        }
        return [
            'id_user'           => 'required',
            'id_fakultas'       => 'required',
            'id_matakuliah'     => 'required',
            'id_tahun_ajaran'   => 'required',
            'id_jenis_ujian'    => 'required',
            'id_kelas'          => 'required',
            'kode_jadwal_ujian' => $kodeJadwalUjian,
            'tanggal_ujian'     => 'required|date',
        ];
    }
}
