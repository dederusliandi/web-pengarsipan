<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table = 'kurikulum';

    protected $fillable = [
        'id_user',
        'id_fakultas',
        'id_matakuliah',
        'id_tahun_ajaran',
        'id_kelas',
        'kode_kurikulum'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function fakultas()
    {
        return $this->belongsTo('App\Fakultas','id_fakultas','id');
    }

    public function matakuliah()
    {
        return $this->belongsTo('App\Matakuliah','id_matakuliah','id');
    }

    public function tahunAjaran()
    {
        return $this->belongsTo('App\TahunAjaran','id_tahun_ajaran','id');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Kelas','id_kelas','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_kurikulum','id');
    }

    public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_kurikulum','id');
    }
}
