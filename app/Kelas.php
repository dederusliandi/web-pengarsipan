<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';

    protected $fillable = [
        'kode_kelas',
        'kelas'
    ];

    public function matakuliah()
    {
        return $this->hasMany('App\Matakuliah','id_matakuliah','id');
    }

    public function kurikulum()
    {
        return $this->hasMany('App\Kurikulum','id_matakuliah','id');
    }

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_kelas','id');
    }
}
