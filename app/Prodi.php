<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $table = 'prodi';

    protected $fillable = [
        'id_user',
        'id_fakultas',
        'kode_prodi',
        'prodi'
    ];

    public function fakultas()
    {
        return $this->belongsTo('App\Fakultas','id_fakultas','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function matakuliah()
    {
        return $this->hasMany('App\Matakuliah','id_prodi','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_prodi','id');
    }

    public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_prodi','id');
    }
}
