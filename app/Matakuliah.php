<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    protected $table = 'matakuliah';

    protected $fillable = [
        'id_user',
        'id_prodi',
        'id_fakultas',
        'id_kelas',
        'kode_matakuliah',
        'matakuliah',
        'sks',
        'semester'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function prodi()
    {
        return $this->belongsTo('App\Prodi','id_prodi','id');
    }

    public function fakultas()
    {
        return $this->belongsTo('App\Fakultas','id_fakultas','id');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Kelas','id_kelas','id');
    }

    public function kurikulum()
    {
        return $this->hasMany('App\Kurikulum','id_matakuliah','id');
    }

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_matakuliah','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_matakuliah','id');
    }

     public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_matakuliah','id');
    }
    
}
