<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request; 

class ActiveLinkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $page = '';

        if(Request::segment(1) == '') {
            $page = 'dashboard';
        }
         
        if(Request::segment(1) == 'user') {
            $page = 'user';
        }

        if(Request::segment(1) == 'fakultas') {
            $page = 'fakultas';
        }

        if(Request::segment(1) == 'prodi') {
            $page = 'prodi';
        }

        if(Request::segment(1) == 'matakuliah') {
            $page = 'matakuliah';
        }

        if(Request::segment(1) == 'tahun-ajaran') {
            $page = 'tahun_ajaran';
        }

        if(Request::segment(1) == 'kurikulum') {
            $page = 'kurikulum';
        }

        if(Request::segment(1) == 'jenis-ujian') {
            $page = 'jenis_ujian';
        }

        if(Request::segment(1) == 'kelas') {
            $page = 'kelas';
        }

        if(Request::segment(1) == 'jadwal-ujian') {
            $page = 'jadwal_ujian';
        }

        if(Request::segment(1) == 'arsip-soal') {
            $page = 'arsip_soal';
        }

        if(Request::segment(1) == 'arsip-nilai') {
            $page = 'arsip_nilai';
        }

        view()->share('page',$page);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
