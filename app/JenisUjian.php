<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUjian extends Model
{
    protected $table = 'jenis_ujian';

    protected $fillable = [
        'kode_jenis_ujian',
        'jenis_ujian'
    ];

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_jenis_ujian','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_jenis_ujian','id');
    }

     public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_jenis_ujian','id');
    }
}
