<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';

    protected $fillable = [
        'id_user',
        'kode_fakultas',
        'fakultas'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function prodi()
    {
        return $this->hasMany('App\Prodi','id_fakultas','id');
    }

    public function matakuliah()
    {
        return $this->hasMany('App\Matakuliah','id_fakultas','id');
    }

    public function kurikulum()
    {
        return $this->hasMany('App\Kurikulum','id_fakultas','id');
    }

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_fakultas','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_fakultas','id');
    }

    public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_fakultas','id');
    }
}
