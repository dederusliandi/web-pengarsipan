<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nama_lengkap','nomor_induk','email', 'password','role_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profil()
    {
        return $this->hasOne('App\Profil','id_user','id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function fakultas()
    {
        return $this->hasMany('App\Fakultas','id_user','id');
    }

    public function prodi()
    {
        return $this->hasMany('App\Prodi','id_user','id');
    }

    public function matakuliah()
    {
        return $this->hasMany('App\Matakuliah','id_user','id');
    }

    public function kurikulum()
    {
        return $this->hasMany('App\Kurikulum','id_user','id');
    }

    public function jadwalUjian()
    {
        return $this->hasMany('App\JadwalUjian','id_user','id');
    }

    public function arsipSoal()
    {
        return $this->hasMany('App\ArsipSoal','id_user','id');
    }

    public function arsipNilai()
    {
        return $this->hasMany('App\ArsipNilai','id_user','id');
    }

}
