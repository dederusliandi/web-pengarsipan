<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipSoal extends Model
{
    protected $table = 'arsip_soal';

    protected $fillable = [
        'id_user',
        'id_kurikulum',
        'id_jenis_ujian',
        'id_tahun_ajaran',
        'id_jadwal_ujian',
        'id_matakuliah',
        'id_fakultas',
        'id_prodi',
        'kode_soal',
        'soal'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function kurikulum()
    {
        return $this->belongsTo('App\Kurikulum','id_kurikulum','id');
    }

    public function jenisUjian()
    {
        return $this->belongsTo('App\JenisUjian','id_jenis_ujian','id');
    }

    public function tahunAjaran()
    {
        return $this->belongsTo('App\TahunAjaran','id_tahun_ajaran','id');
    }

    public function jadwalUjian()
    {
        return $this->belongsTo('App\JadwalUjian','id_jadwal_ujian','id');
    }

    public function matakuliah()
    {
        return $this->belongsTo('App\Matakuliah','id_matakuliah','id');
    }

    public function fakultas()
    {
        return $this->belongsTo('App\Fakultas','id_fakultas','id');
    }

    public function prodi()
    {
        return $this->belongsTo('App\Prodi','id_prodi','id');
    }
}
