<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama_lengkap'  => 'dede rusliandi taruna',
            'nomor_induk'   => 'U0001',
            'email'         => 'dede.rusliandi1@gmail.com',
            'password'      => bcrypt('123456'),
            'role_id'       => 1
        ]);

        DB::table('profil')->insert([
            'id_user'       => 1,
            'jenis_kelamin' => 'laki-laki',
            'tempat_lahir'  => 'bandung',
            'tanggal_lahir' => '2018-01-01',
            'agama'         => 'islam',
            'telepon'       => '081223230946',
            'alamat'        => 'bandung',
            'foto'          => 'dede.jpg'
        ]);
    }
}
