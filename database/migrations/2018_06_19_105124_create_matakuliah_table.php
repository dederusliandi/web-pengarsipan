<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatakuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matakuliah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_prodi')->unsigned();
            $table->integer('id_fakultas')->unsigned();
            $table->integer('id_kelas')->unsigned();
            $table->string('kode_matakuliah')->unique();
            $table->string('matakuliah');
            $table->string('sks');
            $table->enum('semester',['ganjil','genap']);
            $table->timestamps();

            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            
            $table->foreign('id_prodi')
                  ->references('id')
                  ->on('prodi')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');


            $table->foreign('id_fakultas')
                  ->references('id')
                  ->on('fakultas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_kelas')
                  ->references('id')
                  ->on('kelas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matakuliah');
    }
}
