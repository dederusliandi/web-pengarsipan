<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurikulum', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_fakultas')->unsigned();
            $table->integer('id_matakuliah')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_kelas')->unsigned();
            $table->string('kode_kurikulum')->unique();
            $table->timestamps();

            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_fakultas')
                  ->references('id')
                  ->on('fakultas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_matakuliah')
                  ->references('id')
                  ->on('matakuliah')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_tahun_ajaran')
                  ->references('id')
                  ->on('tahun_ajaran')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_kelas')
                  ->references('id')
                  ->on('kelas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurikulum');
    }
}
