<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_ujian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_fakultas')->unsigned();
            $table->integer('id_matakuliah')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_jenis_ujian')->unsigned();
            $table->integer('id_kelas')->unsigned();
            $table->string('kode_jadwal_ujian')->unique();
            $table->date('tanggal_ujian');
            $table->timestamps();

            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_fakultas')
                  ->references('id')
                  ->on('fakultas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_matakuliah')
                  ->references('id')
                  ->on('matakuliah')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_tahun_ajaran')
                  ->references('id')
                  ->on('tahun_ajaran')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_jenis_ujian')
                  ->references('id')
                  ->on('jenis_ujian')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_kelas')
                  ->references('id')
                  ->on('kelas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_ujian');
    }
}
