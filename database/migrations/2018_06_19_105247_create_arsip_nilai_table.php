<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArsipNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsip_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_kurikulum')->unsigned();
            $table->integer('id_jenis_ujian')->unsigned();
            $table->integer('id_tahun_ajaran')->unsigned();
            $table->integer('id_jadwal_ujian')->unsigned();
            $table->integer('id_matakuliah')->unsigned();
            $table->integer('id_fakultas')->unsigned();
            $table->integer('id_prodi')->unsigned();
            $table->string('kode_nilai')->unique();
            $table->string('nilai');
            $table->timestamps();

            $table->foreign('id_tahun_ajaran')
                  ->references('id')
                  ->on('tahun_ajaran')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_kurikulum')
                  ->references('id')
                  ->on('kurikulum')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_jenis_ujian')
                  ->references('id')
                  ->on('jenis_ujian')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_matakuliah')
                  ->references('id')
                  ->on('matakuliah')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_fakultas')
                  ->references('id')
                  ->on('fakultas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_prodi')
                  ->references('id')
                  ->on('prodi')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_jadwal_ujian')
                  ->references('id')
                  ->on('jadwal_ujian')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsip_nilai');
    }
}
